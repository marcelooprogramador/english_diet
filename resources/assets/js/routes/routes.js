import Vue from "vue";
import VueRouter from "vue-router";

//##### IMPORTS REQUIRED COMPONENTS VUE
import TeachersroomComponent from "../components/admin/Teachersroom/TeachersroomComponent";

Vue.use(VueRouter);

const routes = [
  {

    path : "/teachersroom",
    component : TeachersroomComponent,
    name: 'teachersroom'
  
  }
];

const router = new VueRouter({
  mode : 'history',
  routes
})

export default router;