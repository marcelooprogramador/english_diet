require('./bootstrap');

window.Vue = require('vue');

import router from "./routes/routes";

Vue.component('app-component', require('./components/AppComponent.vue'));

const app = new Vue({
    el: '#app',
    router
});
